#include<iostream>
#include"tree.h"
#include<cstdlib>
#include<ctime>
#include<vector>
#include<time.h>
#include<string>

using namespace std;

int main() {
	Tree<int>tree;
	vector<int>data;
	int num , searchNum , delNum;


	srand(time(0));

	for (int i = 0; i < 500; i++) {
		num = rand() % 500;
		tree.insert(num);
		data.push_back(num);
	}
	tree.inorder(); //to print a tree
	
	tree.balance(data, 0, data.size() - 1); 

	cout << "Max depth of node is : " << tree.maxDepth() << endl; 
	cout << "Input Number to search : ";
	cin >> searchNum;
	if (tree.search(searchNum)) { 
		cout << searchNum << " Found" <<endl;
	}
	else {
		cout << searchNum << " Not found"<<endl;
	}

	cout << "What number that you want to delete ? : ";
	cin >> delNum;
	tree.DeleteSearch(delNum); 
	tree.inorder();  
	system("pause");


	return 0;
}