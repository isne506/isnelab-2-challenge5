#include <queue>
#include <stack>
using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node { 
public:
	Node() { left = right = NULL; } //define left,right node = NULL
	Node(const T& el, Node *l = 0, Node *r = 0){
		key = el; left = l; right = r;
	}
	T key;
	Node *left, *right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }

	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; } //function that check that the tree is empty or not
	void inorder() { inorder(root); }
	void insert(const T& el); //insert node to a tree
	void deleteNode(Node<T> *& node); // delete node from the tree
	void visit(Node<T> *p);  //function that show content in node
	void balance(vector<T> data, int first, int last); //function that make the tree balance
	bool search(T num) { return search(root, num); }  //function the search to the tree
	int maxDepth() { return maxDepth(root); };   // function that return max depth
	void DeleteSearch(const T& var);  

protected:
	Node<T> *root;
	void clear(Node<T> *p);
	void inorder(Node<T> *p);
	bool search(Node<T> *p, T num);
	int maxDepth(Node<T> *p);
};

template<class T>
void Tree<T>::clear(Node<T> *p)
{
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T> *p) { // to visit all node and show
	if (p != 0) {  
		inorder(p->left);  
		visit(p);  
		inorder(p->right);
	}
}

template<class T>
void Tree<T>::insert(const T &el) { 
	Node<T> *p = root, *prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el) //if current data < insert el 
			p = p->right; //move right
		else
			p = p->left; //move left
	}
	if (root == 0)
		root = new Node<T>(el); //if dont have root, insert to root
	else if (prev->key < el)
		prev->right = new Node<T>(el); //if parent node < new el , add to right of parent node
	else
		prev->left = new Node<T>(el); //if parent node > new el , add to LEFT of parent node
}

template<class T>
void Tree<T>::deleteNode(Node<T>*& node) {
	Node<T> *prev = 0, *tmp = node;
	if (node != 0) {
		if (node->right == 0)
			node = node->left;
		else if (node->left == 0)
			node = node->right;
		else {
			tmp = node->left;
			prev = node;
			while (tmp->right != 0) {
				prev = tmp;
				tmp = tmp->right;
			}
			node->key = tmp->key;
			if (prev == node)
				prev->left = tmp->left;
			else prev->right = tmp->left;
		}
		delete tmp;
	}
}
template<class T>
void Tree<T>::visit(Node<T> *p)
{
	cout << p->key << endl; // print data in node
}
template<class T>
bool Tree<T>::search(Node<T> *root, T num) {
	if (root == NULL || root->key == num) // if no root or root equal to num 
		return root;  //return root

	if (root->key < num) // if root element less than num
		return search(root->right, num);  //return call search from right

	return search(root->left, num);  //return call search from left
}
template<class T>
int Tree<T>::maxDepth(Node<T> *p)  
{
	if (p == NULL) 
	{
		return 0;//no depth
	}
	else
	{
		int lDepth = maxDepth(p->left);	 //find left depth
		int rDepth = maxDepth(p->right);  //find right depth

		if (lDepth > rDepth) // if left depth more than right depth
			return(lDepth + 1); // return left depth (+1 because the first is 0)  
		else return(rDepth + 1);  // if right depth more than left depth , return right depth
	}
}
template<class T>
void Tree<T>::balance(vector<T> data, int first, int last) {
	if (first <= last) {  
		int middle = (first + last) / 2;  
		insert(data[middle]);  //insert middle 
		balance(data, first, middle - 1); //call balance from less side (left)
		balance(data, middle + 1, last);  //call balance from more side (right)
	}
}
template<class T>
void Tree<T>::DeleteSearch(const T &el) {  // delete by element in node
	Node<T> *node = root, *prev = 0;
	while (node != 0)  // while node not equal to 0
	{
		if (node->key == el) //if element that want to delete equal to node
			break;  // go out this loop
		prev = node;
		if (node->key < el)  
			node = node->right; // if node element < element that want to delete, go right 
		else node = node->left;  //if node element > element that want to delete, go left
	}
	if (node != 0 && node->key == el) //if element in node not equal to 0 and equal to element that want to delete
		if (node == root)  //if the element equal to element of root node
			deleteNode(root);  // delete root node
		else if (prev->left == node) 
			deleteNode(prev->left);  //delete node left
		else
			deleteNode(prev->right);  //delete node right
}

#endif 
